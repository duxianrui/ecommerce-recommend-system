# ECommerceRecommendSystem

#### 项目架构
电商推荐系统项目

![image-20220607155901792](C:\Users\duxianrui\AppData\Roaming\Typora\typora-user-images\image-20220607155901792.png)

主要技术栈包括spark、mongodb、kafka等

#### 主要推荐模块

##### 离线推荐

- 统计推荐

直接统计历史评分数最多以及最近评分数最多的商品作为热门商品推荐

![image-20220607160059499](C:\Users\duxianrui\AppData\Roaming\Typora\typora-user-images\image-20220607160059499.png)

- 基于LFM的离线推荐

使用ALS算法构建预测模型，ALS算法参考：[ALS算法介绍](https://blog.csdn.net/qq_42448606/article/details/109214713)

得到ALS算法构建的模型后，将用户与商品做笛卡尔积，使用模型预测用户商品矩阵的每一个评分然后排序，返回评分最高的K个商品。

![image-20220607162941054](C:\Users\duxianrui\AppData\Roaming\Typora\typora-user-images\image-20220607162941054.png)

ALS算法会为每个商品生成一个特征值，将商品做笛卡尔积计算两两之间的特征相似度保存会mongo

![image-20220607170055324](C:\Users\duxianrui\AppData\Roaming\Typora\typora-user-images\image-20220607170055324.png)

- 基于内容的推荐

基于内容的推荐主要使用的是商品的tag信息，通过对商品标签进行提取得到商品的内容特征向量，进而得到求取商品的相似度矩阵。为了避免热门标签对特征提取的影响，可以通过TF-IDF算法对标签权重进行调整。

![image-20220607173003683](C:\Users\duxianrui\AppData\Roaming\Typora\typora-user-images\image-20220607173003683.png)

- 基于物品的协同过滤相似推荐

基于物品的协同过滤相似推荐主要使用的是用户与商品的交互信息（点击，收藏，购买，评分等），如果两个商品有同样的受众，说明它们之间具有内在相关性，也就是“同现相似度”。因此这种方式的核心是需要找到两个商品的共同评分人数。

![image-20220607173833227](C:\Users\duxianrui\AppData\Roaming\Typora\typora-user-images\image-20220607173833227.png)

##### 实时推荐

实时推荐的依据是用户最近k次的评分，所依据的基本原理是最近一段时间用户的口味是相似的。因此根据用户最近评分的商品，从商品相似度列表中找出所有备选商品。备选商品推荐优先级的计算考虑与最近评分的商品的相似度以及评分的分数两个方面，简单来说就是用户评分高且与这件评分物品相似度高的商品推荐优先级更高。

![image-20220607171449892](C:\Users\duxianrui\AppData\Roaming\Typora\typora-user-images\image-20220607171449892.png)

![image-20220607171521424](C:\Users\duxianrui\AppData\Roaming\Typora\typora-user-images\image-20220607171521424.png)



#### 待处理问题

- 学习sparkstreaming与kafka的联通

- 学习Scala操作redis

- 学习Scala操作mongodb

- 深入理解广播变量

  
