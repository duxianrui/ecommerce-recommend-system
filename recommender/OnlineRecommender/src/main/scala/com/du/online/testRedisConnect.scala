package com.du.online

import redis.clients.jedis.Jedis

import scala.collection.JavaConversions.asScalaBuffer

object testRedisConnect {
  def main(args: Array[String]): Unit = {
    val jedis = new Jedis("81.68.138.86", 6379)
    val result = jedis.lrange("userId:" + 4867.toString, 0, -1)
      .map{ item =>
        val attr = item.split("\\:")
        ( attr(0).trim.toInt, attr(1).trim.toDouble )
      }
      .toArray
    println(result(0))
  }

}
